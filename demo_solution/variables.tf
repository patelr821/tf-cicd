variable "region" {
  description = "AWS region"
  default     = "us-east-1"
}

variable "bitbucket_connection" {
  description = "BitBucket connection"
  default     = "arn:aws:codestar-connections:us-east-1:748939641058:connection/6c1ba577-c0b8-48a9-ab26-683ef7079abf"
}

variable "artifacts_bucket" {
  description = "S3 Bucket to deploy to"
  default     = "itcc2200-artifacts-bucket-20211212185214656300000001"
}

variable "role_arn_cloudformation" {
  description = "Role for cloudformation ARN"
  default     = "arn:aws:iam::748939641058:role/ITCC2X00_Week12_CloudFormation"
}

variable "role_arn_codebuild" {
  description = "Role for CodeBuild ARN"
  default     = "arn:aws:iam::748939641058:role/ITCC2X00_Week12_CodeBuild"
}

variable "role_arn_codedeploy" {
  description = "Role for CodeDeploy ARN"
  default     = "arn:aws:iam::748939641058:role/ITCC2X00_Week12_CodeDeploy"
}

variable "role_arn_codepipeline" {
  description = "Role for CodePipeline ARN"
  default     = "arn:aws:iam::748939641058:role/ITCC2X00_Week12_CodePipeline"
}

variable "role_arn_lambda" {
  description = "Role for Lambda ARN"
  default     = "arn:aws:iam::748939641058:role/ITCC2X00_Week12_Lambda"
}