data "template_file" "buildspec" {
    template = file("../buildspec.yml")
    vars = {
        bucket  = "${var.artifacts_bucket}"
        lambda_role = "${var.role_arn_lambda}"
    }
}

resource "aws_codebuild_project" "dogtreat_codebuild" {
    name = "roshani_lambda_build"
    service_role = var.role_arn_codebuild
    artifacts {
        name = "roshani_lambda_artifact"
        type = "CODEPIPELINE"
    }

    environment {
        compute_type = "BUILD_GENERAL1_SMALL"
        image = "aws/codebuild/amazonlinux2-x86_64-standard:3.0"
        type = "LINUX_CONTAINER"
    }

    logs_config {
        cloudwatch_logs{
            group_name = "code_build_log_group"
        }
    }

    source {
        buildspec = data.template_file.buildspec.rendered
        type = "CODEPIPELINE"
    }
}