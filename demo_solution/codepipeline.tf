resource "aws_codepipeline" "roshani_final" {
  name     = "roshani-lambdas"
  role_arn = var.role_arn_codepipeline

  artifact_store {
    location = var.artifacts_bucket
    type     = "S3"
  }

  stage {
    name = "Source"
    action {
      name             = "GetSource"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeStarSourceConnection"
      version          = "1"
      output_artifacts = ["source_output"]
      run_order        = 1

      configuration = {
        ConnectionArn    = "${var.bitbucket_connection}"
        FullRepositoryId = "patelr821/tf-cicd.git"
        BranchName       = "master"
      }
    }
  }

  stage {
    name = "Test-N-Build"

    action {
      name             = "TestAndBuild"
      category         = "Test"
      owner            = "AWS"
      provider         = "CodeBuild"
      input_artifacts  = ["source_output"]
      output_artifacts = ["build_output"]
      version          = "1"
      run_order        =  2

      configuration = {
        ProjectName = "roshani_lambda_build"
      }
    }
  }

  stage {
    name = "Deploy"

    action {
      name            = "DeployLambda"
      category        = "Deploy"
      owner           = "AWS"
      provider        = "CloudFormation"
      input_artifacts = ["build_output"]
      version         = "1"
      run_order       = 3

      configuration = {
        StackName      = "RoshaniStack"
        ActionMode     = "REPLACE_ON_FAILURE"
        Capabilities   = "CAPABILITY_AUTO_EXPAND,CAPABILITY_IAM"
        OutputFileName = "CreateStackOutput.json"
        RoleArn      = "${var.role_arn_cloudformation}"
        TemplatePath   = "build_output::template.yaml"
      }
    }
  }
}